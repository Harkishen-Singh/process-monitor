"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 *  main process for process monitor
 */
var electron_1 = require("electron");
var winMain;
electron_1.app.on('ready', function () {
    winMain = new electron_1.BrowserWindow({
        height: 600,
        width: 800,
        backgroundColor: '#fff'
    });
    winMain.on('closed', function () {
        winMain = null;
    });
    winMain.loadURL('file://' + __dirname + '/processes/templates/index.html');
    winMain.webContents.openDevTools();
    electron_1.Menu.setApplicationMenu(null);
});
