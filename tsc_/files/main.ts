/**
 *  main process for process monitor
 */
 import { BrowserWindow, Menu, app, IpcMain } from 'electron';
 

 let winMain;

 app.on('ready', () => {
     winMain = new BrowserWindow({
         height: 600,
         width: 800,
         backgroundColor: '#fff'
     });
     winMain.on('closed', () =>{
         winMain = null;
     });
     winMain.loadURL('file://'+__dirname+'/processes/templates/index.html')
     winMain.webContents.openDevTools();
     Menu.setApplicationMenu(null);
 });
 
 